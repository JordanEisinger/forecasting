# U2P, Seeking, WYP and Other Revenue Forecasting

## Goal

* Accurately project daily, weekly, monthly, and year-end revenue figures for all Reflex Media sites in aggregate as well as individually
* Accurately project revenue attribution breakouts for each of the core sites (i.e. SEM, Non-SEM, etc.)

## About

**Author**: *Jordan Eisinger, Data Scientist* \
**Created On**: *01/01/2023* \
**Last Updated On**: *01/24/2023*

**TEST COMMIT**
